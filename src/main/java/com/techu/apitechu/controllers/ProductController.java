package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController //Primero identificamos como Controller
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";


    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");
        //return new ArrayList<>();
        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id es " + id);

        ProductModel result =  new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)) {
                result = product;
            }
        }

        //return new ProductModel();
        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es " + newProduct.getId());
        System.out.println("La descripción del nuevo producto es " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        //return new ProductModel();
        return  newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("La id en parámetro de la url es " + id);
        System.out.println("La id del producto a actualizar " + product.getId());
        System.out.println("La descripción del producto a actualizar " + product.getDesc());
        System.out.println("El precio del producto a actualizar " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        //return new ProductModel();
        return product;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a eliminar es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                ApitechuApplication.productModels.remove(product);
                result = product;
            }
        }
        //return new ProductModel();
        return result;
    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel PatchProduct(@RequestBody ProductModel product,@PathVariable String id) {
        System.out.println("patchProduct");
        System.out.println("La id en parámetro de la url es " + id);
        System.out.println("La id del producto a actualizar " + product.getId());
        System.out.println("La descripción del producto a actualizar " + product.getDesc());
        System.out.println("El precio del producto a actualizar " + product.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                result = productInList;

                if (product.getDesc() != null) {
                    System.out.println("Actualizando descripción");
                    productInList.setDesc(product.getDesc());
                }
                if (product.getPrice() != 0.0) {
                    System.out.println("Actualizando precio");
                    productInList.setPrice(product.getPrice());
                }
            }
        }

        return result;
    }
}
